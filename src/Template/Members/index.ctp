<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Member'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Messages'), ['controller' => 'Messages', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Message'), ['controller' => 'Messages', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Subscriptions'), ['controller' => 'Subscriptions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Subscription'), ['controller' => 'Subscriptions', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tl Member To Group'), ['controller' => 'TlMemberToGroup', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tl Member To Group'), ['controller' => 'TlMemberToGroup', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Archive Bands'), ['controller' => 'ArchiveBands', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Archive Band'), ['controller' => 'ArchiveBands', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Bands Members Instruments'), ['controller' => 'BandsMembersInstruments', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Bands Members Instrument'), ['controller' => 'BandsMembersInstruments', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Bands'), ['controller' => 'Bands', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Band'), ['controller' => 'Bands', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Supplies'), ['controller' => 'Supplies', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Supply'), ['controller' => 'Supplies', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="members index large-9 medium-8 columns content">
    <h3><?= __('Members') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('name') ?></th>
                <th><?= $this->Paginator->sort('alias') ?></th>
                <th><?= $this->Paginator->sort('is_board_member') ?></th>
                <th><?= $this->Paginator->sort('board_role') ?></th>
                <th><?= $this->Paginator->sort('is_musician') ?></th>
                <th><?= $this->Paginator->sort('is_volunteer') ?></th>
                <th><?= $this->Paginator->sort('mobile') ?></th>
                <th><?= $this->Paginator->sort('phone') ?></th>
                <th><?= $this->Paginator->sort('email') ?></th>
                <th><?= $this->Paginator->sort('birthday') ?></th>
                <th><?= $this->Paginator->sort('has_room_keys') ?></th>
                <th><?= $this->Paginator->sort('has_board_keys') ?></th>
                <th><?= $this->Paginator->sort('is_active') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th><?= $this->Paginator->sort('disabled') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($members as $member): ?>
            <tr>
                <td><?= $this->Number->format($member->id) ?></td>
                <td><?= h($member->name) ?></td>
                <td><?= h($member->alias) ?></td>
                <td><?= h($member->is_board_member) ?></td>
                <td><?= h($member->board_role) ?></td>
                <td><?= h($member->is_musician) ?></td>
                <td><?= h($member->is_volunteer) ?></td>
                <td><?= h($member->mobile) ?></td>
                <td><?= h($member->phone) ?></td>
                <td><?= h($member->email) ?></td>
                <td><?= h($member->birthday) ?></td>
                <td><?= h($member->has_room_keys) ?></td>
                <td><?= h($member->has_board_keys) ?></td>
                <td><?= h($member->is_active) ?></td>
                <td><?= h($member->created) ?></td>
                <td><?= h($member->modified) ?></td>
                <td><?= h($member->disabled) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $member->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $member->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $member->id], ['confirm' => __('Are you sure you want to delete # {0}?', $member->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
