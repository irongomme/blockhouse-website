<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Member'), ['action' => 'edit', $member->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Member'), ['action' => 'delete', $member->id], ['confirm' => __('Are you sure you want to delete # {0}?', $member->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Members'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Member'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Messages'), ['controller' => 'Messages', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Message'), ['controller' => 'Messages', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Subscriptions'), ['controller' => 'Subscriptions', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Subscription'), ['controller' => 'Subscriptions', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tl Member To Group'), ['controller' => 'TlMemberToGroup', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tl Member To Group'), ['controller' => 'TlMemberToGroup', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Archive Bands'), ['controller' => 'ArchiveBands', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Archive Band'), ['controller' => 'ArchiveBands', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Bands Members Instruments'), ['controller' => 'BandsMembersInstruments', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Bands Members Instrument'), ['controller' => 'BandsMembersInstruments', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Bands'), ['controller' => 'Bands', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Band'), ['controller' => 'Bands', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Supplies'), ['controller' => 'Supplies', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Supply'), ['controller' => 'Supplies', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="members view large-9 medium-8 columns content">
    <h3><?= h($member->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($member->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Alias') ?></th>
            <td><?= h($member->alias) ?></td>
        </tr>
        <tr>
            <th><?= __('Board Role') ?></th>
            <td><?= h($member->board_role) ?></td>
        </tr>
        <tr>
            <th><?= __('Mobile') ?></th>
            <td><?= h($member->mobile) ?></td>
        </tr>
        <tr>
            <th><?= __('Phone') ?></th>
            <td><?= h($member->phone) ?></td>
        </tr>
        <tr>
            <th><?= __('Email') ?></th>
            <td><?= h($member->email) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($member->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Birthday') ?></th>
            <td><?= h($member->birthday) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($member->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($member->modified) ?></td>
        </tr>
        <tr>
            <th><?= __('Disabled') ?></th>
            <td><?= h($member->disabled) ?></td>
        </tr>
        <tr>
            <th><?= __('Is Board Member') ?></th>
            <td><?= $member->is_board_member ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th><?= __('Is Musician') ?></th>
            <td><?= $member->is_musician ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th><?= __('Is Volunteer') ?></th>
            <td><?= $member->is_volunteer ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th><?= __('Has Room Keys') ?></th>
            <td><?= $member->has_room_keys ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th><?= __('Has Board Keys') ?></th>
            <td><?= $member->has_board_keys ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th><?= __('Is Active') ?></th>
            <td><?= $member->is_active ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Comment') ?></h4>
        <?= $this->Text->autoParagraph(h($member->comment)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Messages') ?></h4>
        <?php if (!empty($member->messages)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Member Id') ?></th>
                <th><?= __('Method') ?></th>
                <th><?= __('Reference') ?></th>
                <th><?= __('Status') ?></th>
                <th><?= __('Message') ?></th>
                <th><?= __('Destination') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th><?= __('Log') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($member->messages as $messages): ?>
            <tr>
                <td><?= h($messages->id) ?></td>
                <td><?= h($messages->member_id) ?></td>
                <td><?= h($messages->method) ?></td>
                <td><?= h($messages->reference) ?></td>
                <td><?= h($messages->status) ?></td>
                <td><?= h($messages->message) ?></td>
                <td><?= h($messages->destination) ?></td>
                <td><?= h($messages->created) ?></td>
                <td><?= h($messages->modified) ?></td>
                <td><?= h($messages->log) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Messages', 'action' => 'view', $messages->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Messages', 'action' => 'edit', $messages->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Messages', 'action' => 'delete', $messages->id], ['confirm' => __('Are you sure you want to delete # {0}?', $messages->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Subscriptions') ?></h4>
        <?php if (!empty($member->subscriptions)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Member Id') ?></th>
                <th><?= __('Amount') ?></th>
                <th><?= __('Payment Method') ?></th>
                <th><?= __('Payment Reference') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Status') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($member->subscriptions as $subscriptions): ?>
            <tr>
                <td><?= h($subscriptions->id) ?></td>
                <td><?= h($subscriptions->member_id) ?></td>
                <td><?= h($subscriptions->amount) ?></td>
                <td><?= h($subscriptions->payment_method) ?></td>
                <td><?= h($subscriptions->payment_reference) ?></td>
                <td><?= h($subscriptions->created) ?></td>
                <td><?= h($subscriptions->status) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Subscriptions', 'action' => 'view', $subscriptions->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Subscriptions', 'action' => 'edit', $subscriptions->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Subscriptions', 'action' => 'delete', $subscriptions->id], ['confirm' => __('Are you sure you want to delete # {0}?', $subscriptions->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Tl Member To Group') ?></h4>
        <?php if (!empty($member->tl_member_to_group)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Tstamp') ?></th>
                <th><?= __('Member Id') ?></th>
                <th><?= __('Group Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($member->tl_member_to_group as $tlMemberToGroup): ?>
            <tr>
                <td><?= h($tlMemberToGroup->id) ?></td>
                <td><?= h($tlMemberToGroup->tstamp) ?></td>
                <td><?= h($tlMemberToGroup->member_id) ?></td>
                <td><?= h($tlMemberToGroup->group_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'TlMemberToGroup', 'action' => 'view', $tlMemberToGroup->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'TlMemberToGroup', 'action' => 'edit', $tlMemberToGroup->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'TlMemberToGroup', 'action' => 'delete', $tlMemberToGroup->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tlMemberToGroup->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Users') ?></h4>
        <?php if (!empty($member->users)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Member Id') ?></th>
                <th><?= __('Role Id') ?></th>
                <th><?= __('Email') ?></th>
                <th><?= __('Password') ?></th>
                <th><?= __('Role') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th><?= __('Is Active') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($member->users as $users): ?>
            <tr>
                <td><?= h($users->id) ?></td>
                <td><?= h($users->member_id) ?></td>
                <td><?= h($users->role_id) ?></td>
                <td><?= h($users->email) ?></td>
                <td><?= h($users->password) ?></td>
                <td><?= h($users->role) ?></td>
                <td><?= h($users->created) ?></td>
                <td><?= h($users->modified) ?></td>
                <td><?= h($users->is_active) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Users', 'action' => 'view', $users->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Users', 'action' => 'edit', $users->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Users', 'action' => 'delete', $users->id], ['confirm' => __('Are you sure you want to delete # {0}?', $users->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Archive Bands') ?></h4>
        <?php if (!empty($member->archive_bands)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Alias') ?></th>
                <th><?= __('Abstract') ?></th>
                <th><?= __('Promotion') ?></th>
                <th><?= __('Logo') ?></th>
                <th><?= __('Pictures') ?></th>
                <th><?= __('Songs') ?></th>
                <th><?= __('Facebook') ?></th>
                <th><?= __('Bandcamp') ?></th>
                <th><?= __('Comment') ?></th>
                <th><?= __('Is Active') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th><?= __('Disabled') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($member->archive_bands as $archiveBands): ?>
            <tr>
                <td><?= h($archiveBands->id) ?></td>
                <td><?= h($archiveBands->name) ?></td>
                <td><?= h($archiveBands->alias) ?></td>
                <td><?= h($archiveBands->abstract) ?></td>
                <td><?= h($archiveBands->promotion) ?></td>
                <td><?= h($archiveBands->logo) ?></td>
                <td><?= h($archiveBands->pictures) ?></td>
                <td><?= h($archiveBands->songs) ?></td>
                <td><?= h($archiveBands->facebook) ?></td>
                <td><?= h($archiveBands->bandcamp) ?></td>
                <td><?= h($archiveBands->comment) ?></td>
                <td><?= h($archiveBands->is_active) ?></td>
                <td><?= h($archiveBands->created) ?></td>
                <td><?= h($archiveBands->modified) ?></td>
                <td><?= h($archiveBands->disabled) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ArchiveBands', 'action' => 'view', $archiveBands->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ArchiveBands', 'action' => 'edit', $archiveBands->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ArchiveBands', 'action' => 'delete', $archiveBands->id], ['confirm' => __('Are you sure you want to delete # {0}?', $archiveBands->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Bands Members Instruments') ?></h4>
        <?php if (!empty($member->bands_members_instruments)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Bands Member Id') ?></th>
                <th><?= __('Instrument Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($member->bands_members_instruments as $bandsMembersInstruments): ?>
            <tr>
                <td><?= h($bandsMembersInstruments->id) ?></td>
                <td><?= h($bandsMembersInstruments->bands_member_id) ?></td>
                <td><?= h($bandsMembersInstruments->instrument_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'BandsMembersInstruments', 'action' => 'view', $bandsMembersInstruments->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'BandsMembersInstruments', 'action' => 'edit', $bandsMembersInstruments->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'BandsMembersInstruments', 'action' => 'delete', $bandsMembersInstruments->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bandsMembersInstruments->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Bands') ?></h4>
        <?php if (!empty($member->bands)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Responsible Id') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Alias') ?></th>
                <th><?= __('Room') ?></th>
                <th><?= __('Abstract') ?></th>
                <th><?= __('Promotion') ?></th>
                <th><?= __('Logo') ?></th>
                <th><?= __('Pictures') ?></th>
                <th><?= __('Songs') ?></th>
                <th><?= __('Facebook') ?></th>
                <th><?= __('Bandcamp') ?></th>
                <th><?= __('Comment') ?></th>
                <th><?= __('Is Active') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th><?= __('Disabled') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($member->bands as $bands): ?>
            <tr>
                <td><?= h($bands->id) ?></td>
                <td><?= h($bands->responsible_id) ?></td>
                <td><?= h($bands->name) ?></td>
                <td><?= h($bands->alias) ?></td>
                <td><?= h($bands->room) ?></td>
                <td><?= h($bands->abstract) ?></td>
                <td><?= h($bands->promotion) ?></td>
                <td><?= h($bands->logo) ?></td>
                <td><?= h($bands->pictures) ?></td>
                <td><?= h($bands->songs) ?></td>
                <td><?= h($bands->facebook) ?></td>
                <td><?= h($bands->bandcamp) ?></td>
                <td><?= h($bands->comment) ?></td>
                <td><?= h($bands->is_active) ?></td>
                <td><?= h($bands->created) ?></td>
                <td><?= h($bands->modified) ?></td>
                <td><?= h($bands->disabled) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Bands', 'action' => 'view', $bands->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Bands', 'action' => 'edit', $bands->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Bands', 'action' => 'delete', $bands->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bands->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Supplies') ?></h4>
        <?php if (!empty($member->supplies)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Quantity') ?></th>
                <th><?= __('Picture') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($member->supplies as $supplies): ?>
            <tr>
                <td><?= h($supplies->id) ?></td>
                <td><?= h($supplies->name) ?></td>
                <td><?= h($supplies->quantity) ?></td>
                <td><?= h($supplies->picture) ?></td>
                <td><?= h($supplies->created) ?></td>
                <td><?= h($supplies->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Supplies', 'action' => 'view', $supplies->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Supplies', 'action' => 'edit', $supplies->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Supplies', 'action' => 'delete', $supplies->id], ['confirm' => __('Are you sure you want to delete # {0}?', $supplies->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
