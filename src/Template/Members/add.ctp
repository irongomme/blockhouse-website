<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Members'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Messages'), ['controller' => 'Messages', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Message'), ['controller' => 'Messages', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Subscriptions'), ['controller' => 'Subscriptions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Subscription'), ['controller' => 'Subscriptions', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tl Member To Group'), ['controller' => 'TlMemberToGroup', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tl Member To Group'), ['controller' => 'TlMemberToGroup', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Archive Bands'), ['controller' => 'ArchiveBands', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Archive Band'), ['controller' => 'ArchiveBands', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Bands Members Instruments'), ['controller' => 'BandsMembersInstruments', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Bands Members Instrument'), ['controller' => 'BandsMembersInstruments', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Bands'), ['controller' => 'Bands', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Band'), ['controller' => 'Bands', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Supplies'), ['controller' => 'Supplies', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Supply'), ['controller' => 'Supplies', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="members form large-9 medium-8 columns content">
    <?= $this->Form->create($member) ?>
    <fieldset>
        <legend><?= __('Add Member') ?></legend>
        <?php
            echo $this->Form->input('name');
            echo $this->Form->input('alias');
            echo $this->Form->input('is_board_member');
            echo $this->Form->input('board_role');
            echo $this->Form->input('is_musician');
            echo $this->Form->input('is_volunteer');
            echo $this->Form->input('mobile');
            echo $this->Form->input('phone');
            echo $this->Form->input('email');
            echo $this->Form->input('birthday', ['empty' => true]);
            echo $this->Form->input('has_room_keys');
            echo $this->Form->input('has_board_keys');
            echo $this->Form->input('comment');
            echo $this->Form->input('is_active');
            echo $this->Form->input('disabled', ['empty' => true]);
            echo $this->Form->input('archive_bands._ids', ['options' => $archiveBands]);
            echo $this->Form->input('bands_members_instruments._ids', ['options' => $bandsMembersInstruments]);
            echo $this->Form->input('bands._ids', ['options' => $bands]);
            echo $this->Form->input('supplies._ids', ['options' => $supplies]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
