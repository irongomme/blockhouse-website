<!-- =========================
   About Us
============================== -->
<section id="block" class="about-us">
    <div class="overlay">
        <div class="container padding-top-large">
            <h2>
                <strong class="bold-text">Le </strong>
                <span class="light-text main-color">BLOCK</span>
            </h2>
            <div class="line main-bg"></div>
            <div class="row margin-bottom-medium">
                <div class="col-md-7">
                    <div class="jumbo-text light-text margin-top-medium wow slideInLeft" data-wow-duration="2s">
                        <strong class="bold-text">Blockhouse Musique</strong> :
                        Association qui promouvoit les <strong class="bold-text">musiques actuelles</strong>
                    </div>
                </div>
                <div class="col-md-5">
                    <img src="img/about-side-side.png" alt="About Us Big Image" class="center-block img-responsive">
                </div>
                <div class="clearfix"></div>
            </div>
            <p class="margin-bottom-medium wow slideInUp">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>
    </div>
</section> <!-- *** end About Us *** -->
