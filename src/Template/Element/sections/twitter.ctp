<!-- =========================
   Twitter
============================== -->
<section id="twitter" class="twitter">
    <div class="overlay">
        <div class="container padding-large text-center">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="icon hvr-grow">
                        <i class="fa fa-twitter"></i>
                    </div>
                    <div class="tweet-text" id="tweets">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> <!-- *** end Twitter *** -->
