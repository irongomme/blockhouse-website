<!-- =========================
   Our Skills
============================== -->
<section id="our-skills" class="our-skills">
    <div class="container padding-top-large">
        <h2>
            Our
            <strong class="bold-text">Skills</strong>
            <span class="light-text main-color">Are</span>
        </h2>
        <div class="line main-bg margin-bottom-medium"></div>
        <div class="row">
            <div class="col-md-7">
                <ul class="skill-bar">
                    <li>
                        <p>Print Design</p>
                        <div class="wrapper"><span data-width="90"></span></div>
                    </li>
                    <li>
                        <p>Web Design</p>
                        <div class="wrapper"><span data-width="80"></span></div>
                    </li>
                    <li>
                        <p>Mobile App Design</p>
                        <div class="wrapper"><span data-width="85"></span></div>
                    </li>
                    <li>
                        <p>Creativity</p>
                        <div class="wrapper"><span data-width="65"></span></div>
                    </li>
                </ul>
            </div>
            <div class="col-md-5">
                <img src="img/skill-bg.jpg" class="center-block img-responsive" alt="Our Skills are excellent">
            </div>
            <div class="clearfix"></div>
        </div> <!-- *** end row *** -->
    </div> <!-- *** end container *** -->
</section> <!-- *** end Our Skills *** -->
