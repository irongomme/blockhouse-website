<!-- =========================
   Testimonial
============================== -->
<section id="testimonial" class="testimonial padding-large white-color text-center">
    <div class="container">
        <div class="row">
            <h2 class="margin-bottom-medium">What Our <strong class="bold-text">Customer</strong> Said</h2>
            <div class="col-md-10 col-md-offset-1">

                <!-- *****  Carousel start ***** -->
                <div id="testimonial-carousel" class="owl-carousel owl-theme testimonial-carousel">

                    <!-- =========================
                       Single Testimonial item
                    ============================== -->
                    <div class="item margin-bottom-small"> <!-- ITEM START -->
                        <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit..</p>
                        <div class="client margin-top-medium clearfix">
                            <img src="img/testimonial/testimonial-1.jpg" height="50" width="50" alt="Client Image">
                            <ul class="client-info main-color">
                                <li><strong>John Doe</strong></li>
                                <li>Co-Founder, Envato</li>
                            </ul>
                        </div>
                    </div> <!-- ITEM END -->

                    <!-- =========================
                       Single Testimonial item
                    ============================== -->
                    <div class="item"> <!-- ITEM START -->
                        <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit..</p>
                        <div class="client margin-top-medium">
                            <img src="img/testimonial/testimonial-2.jpg" alt="Client Image" class="grayscale">
                            <ul class="client-info main-color">
                                <li>John Doe</li>
                                <li>Co-Founder, Envato</li>
                            </ul>
                        </div>
                    </div> <!-- ITEM END -->

                    <div class="item"> <!-- ITEM START -->
                        <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit..</p>
                        <div class="client margin-top-medium">
                            <img src="img/testimonial/testimonial-1.jpg" alt="Client Image" class="grayscale">
                            <ul class="client-info main-color">
                                <li>John Doe</li>
                                <li>Co-Founder, Envato</li>
                            </ul>
                        </div>
                    </div> <!-- ITEM END -->

                    <!-- =========================
                       Single Testimonial item
                    ============================== -->
                    <div class="item"> <!-- ITEM START -->
                        <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit..</p>
                        <div class="client margin-top-medium">
                            <img src="img/testimonial/testimonial-2.jpg" alt="Client Image" class="grayscale">
                            <ul class="client-info main-color">
                                <li>John Doe</li>
                                <li>Co-Founder, Envato</li>
                            </ul>
                        </div>
                    </div> <!-- ITEM END -->

                    <!-- =========================
                       Single Testimonial item
                    ============================== -->
                    <div class="item"> <!-- ITEM START -->
                        <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit..</p>
                        <div class="client margin-top-medium">
                            <img src="img/testimonial/testimonial-1.jpg" alt="Client Image" class="grayscale">
                            <ul class="client-info main-color">
                                <li>John Doe</li>
                                <li>Co-Founder, Envato</li>
                            </ul>
                        </div>
                    </div> <!-- ITEM END -->
                </div>
            </div>
        </div>
    </div>
</section> <!-- *** end Testimonial *** -->
