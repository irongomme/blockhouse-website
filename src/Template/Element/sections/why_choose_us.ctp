<!-- =========================
   Why Coose Us
============================== -->
<section id="why-choose-us" class="why-choose-us">
    <div class="container margin-top-large">
        <h2>
            Why
            <strong class="bold-text">Choose</strong>
            <span class="light-text main-color">Us</span>
        </h2>
        <div class="line main-bg margin-bottom-large"></div>

        <div class="row text-center">

            <!-- *****  Service Single ***** -->
            <div class="col-md-4">
                <div class="service wow slideInLeft">
                    <div class="icon"><i class="icon-idea"></i></div>
                    <h4>Minimal <strong>Design</strong></h4>
                    <p>Suspendisse velit ticol sodales, viverra sigirton vitae, accumsan orci mauris nec</p>
                </div>
            </div>

            <!-- *****  Service Single ***** -->
            <div class="col-md-4">
                <div class="service wow fadeInUp">
                    <div class="icon"><i class="icon-heart"></i></div>
                    <h4>Love Our <strong>Clients</strong></h4>
                    <p>Suspendisse velit ticol sodales, viverra sigirton vitae, accumsan orci mauris nec</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="service wow slideInRight">
                    <div class="icon"><i class="icon-office"></i></div>
                    <h4><strong>Professional</strong> Design</h4>
                    <p>Suspendisse velit ticol sodales, viverra sigirton vitae, accumsan orci mauris nec</p>
                </div>
            </div>

            <!-- *****  Service Single ***** -->
            <div class="col-md-4">
                <div class="service wow slideInLeft">
                    <div class="icon"><i class="icon-mobile"></i></div>
                    <h4><strong>Mobile</strong> App Design</h4>
                    <p>Suspendisse velit ticol sodales, viverra sigirton vitae, accumsan orci mauris nec</p>
                </div>
            </div>

            <!-- *****  Service Single ***** -->
            <div class="col-md-4">
                <div class="service wow fadeInUp">
                    <div class="icon"><i class="icon-code"></i></div>
                    <h4><strong>Web</strong> Development</h4>
                    <p>Suspendisse velit ticol sodales, viverra sigirton vitae, accumsan orci mauris nec</p>
                </div>
            </div>

            <!-- *****  Service Single ***** -->
            <div class="col-md-4">
                <div class="service wow slideInRight">
                    <div class="icon"><i class="icon-web-browser"></i></div>
                    <h4><strong>Web</strong> Design</h4>
                    <p>Suspendisse velit ticol sodales, viverra sigirton vitae, accumsan orci mauris nec</p>
                </div>
            </div>
            <div class="clearfix"></div>
        </div> <!-- *** end row *** -->
    </div> <!-- *** end container *** -->
</section> <!-- *** end Why Choose Us *** -->
