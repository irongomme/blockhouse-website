<!-- =========================
   Send Message
============================== -->
<section id="contact" class="send-message main-bg white-color text-center">
    <div class="send-icon" data-toggle="modal" data-target="#contact-form">
        <i class="fa fa-paper-plane"></i>
    </div>
    <p class="light-text" data-toggle="modal" data-target="#contact-form">
        Un <span class="bold-text">message</span>? Un <span class="bold-text">projet</span > ?
    </p>

    <!-- Contact Form Modal -->
    <div class="modal fade contact-form" id="contact-form" tabindex="-1" role="dialog" aria-labelledby="contact-form" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <div class="modal-body">

                    <!-- *****  Contact form ***** -->
                    <form class="form" id="contact-form">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" id="first-name" placeholder="Prénom">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" id="last-name" placeholder="Nom">
                            </div>
                            <div class="form-group col-md-12">
                                <input type="email" class="form-control" id="email" placeholder="Addresse mail">
                            </div>
                            <div class="form-group col-md-12 mab-none">
                                <textarea rows="6" class="form-control" id="description" placeholder="Message ..."></textarea>
                            </div>
                            <div class="form-group col-md-12">
                                <button class="button bold-text main-bg"><i class="fa fa-paper-plane"></i></button>
                            </div>
                        </div>
                    </form>
                </div> <!-- *** end modal-body *** -->
            </div> <!-- *** end modal-content *** -->
        </div> <!-- *** end modal-dialog *** -->
    </div> <!-- *** end Contact Form modal *** -->
</section> <!-- *** end Send Message *** -->
