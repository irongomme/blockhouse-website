<!-- =========================
   Promote
============================== -->
<section id="promote" class="promote main-bg white-color">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-lg-offset-1 col-md-5 col-sm-4 text-center">
                <p class="light-text">Hire us! We make you smile :)</p>
            </div>
            <div class="col-lg-6 col-lg-offset-1 col-md-7 col-sm-8 button-container">
                <a href="#" class="button deep hvr-grow">Request a Quote</a>
                <span class="margin-right-small margin-left-small">or</span>
                <a href="#" class="button light hvr-grow">Hire Us</a>
            </div>
        </div>
    </div>
</section> <!-- *** end promote *** -->
