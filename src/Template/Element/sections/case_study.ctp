<!-- =========================
   Case Study
============================== -->
<section id="case-study" class="case-study">
    <div class="row mar-none mat-none">

        <!-- *****  Case Study Left ***** -->
        <div class="col-md-6 case-study-left wow slideInLeft">
            <div class="overlay padding-large text-right">
                <div class="description">
                    <h3 class="margin-bottom-small light-text">We are revoulutionizing marketing.</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                </div>
            </div>
        </div>

        <!-- *****  Case Study Right ***** -->
        <div class="col-md-6 case-study-right wow slideInRight">
            <div class="overlay padding-large">
                <div class="description">
                    <h3 class="margin-bottom-small light-text">We are revoulutionizing marketing.</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                </div>
            </div>
        </div>
    </div>
</section> <!-- *** end Case Study *** -->
