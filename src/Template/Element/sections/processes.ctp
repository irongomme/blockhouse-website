<!-- =========================
   Processes
============================== -->
<section id="processes" class="processes">
    <div class="overlay">
        <div class="container padding-large">
            <div class="row">
                <div class="col-md-5 text-center process-interactive wow fadeInLeft" data-wow-duration="2s">
                    <div class="process-bar main-bg discussion">
                        <i class="icon-discussion"></i>
                    </div>
                    <div class="process-bar right check">
                        <i class="icon-check"></i>
                    </div>
                    <div class="lines"></div>
                    <div class="process-bar main-bg idea">
                        <i class="icon-idea"></i>
                    </div>
                    <div class="process-bar right office">
                        <i class="icon-office"></i>
                    </div>
                </div> <!-- *** end col-md-5 *** -->
                <div class="col-md-7">

                    <!-- *****  Single feature ***** -->
                    <div class="feature wow fadeInUp" data-wow-delay=".2s">
                        <div class="icon-container pull-left">
                            <span class="icon-discussion"></span>
                        </div>
                        <div class="description text-left pull-right">
                            <h4><strong>Discussion</strong></h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.
                            </p>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <!-- *****  Single feature ***** -->
                    <div class="feature wow fadeInUp" data-wow-delay=".3s">
                        <div class="icon-container pull-left">
                            <span class="icon-idea "></span>
                        </div>
                        <div class="description text-left pull-right">
                            <h4><strong>Get Idea</strong></h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.
                            </p>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <!-- *****  Single feature ***** -->
                    <div class="feature wow fadeInUp" data-wow-delay=".4s">
                        <div class="icon-container pull-left">
                            <span class="icon-office "></span>
                        </div>
                        <div class="description text-left pull-right">
                            <h4><strong>Implementation</strong></h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.
                            </p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div> <!-- *** end col-md-7 *** -->
            </div>
        </div>
    </div>
</section> <!-- *** end Processes *** -->
