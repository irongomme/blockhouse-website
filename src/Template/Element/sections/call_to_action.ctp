<!-- =========================
   Call to action
============================== -->
<section id="call-to-action" class="call-to-action main-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-1 col-sm-6 col-sm-offset-1 col-xs-12 wow slideInLeft animated">
                <p class="light-text">Like What You See? We're Just Getting Started</p>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 button-container wow slideInRight animated">
                <a href="#portfolio" class="button light internal-link pull-left hvr-grow" data-rel="#portfolio">View Portfolio</a>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
<!-- *** end call-to-action *** -->
