<!-- =========================
   We are  hiring
============================== -->
<section id="we-are-hiring" class="we-are-hiring">
    <div class="container padding-large">
        <div class="row">
            <div class="col-md-7 col-sm-6 wow fadeInLeft">
                <div class="row">
                    <div class="col-md-6">
                        <h2>We Are <span class="main-color bold-text">Hiring Creative</span> Peoples</h2>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <p class="margin-top-medium">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
            <div class="col-md-5 col-sm-6 wow fadeInUp">
                <div class="upload-cv text-center">
                    <div class="inner">
                        <div class="icon">
                            <i class="icon-add-user"></i>
                        </div>
                        <div class="blas">
                            Drop Your CV
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> <!-- *** end We Are Hiring *** -->
