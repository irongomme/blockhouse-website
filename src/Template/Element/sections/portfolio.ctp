<!-- =========================
   Portfolio
============================== -->
<section id="portfolio" class="portfolio padding-large text-center">
    <div class="container margin-bottom-medium">
        <div class="row margin-bottom-medium wow fadeInUp">
            <h2>
                Our
                <strong class="main-color bold-text">Portfolios</strong>
            </h2>
            <div class="line main-bg"></div>

            <div class="col-md-10 col-md-offset-1">
                <div class="subtitle">Check out some of our latest and greatest projects.</div>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, illum, soluta. Minima vitae laudantium, sint officiis repellendus. Minima vitae laudantium, sint officiis repellendus</p>
            </div>
        </div> <!-- *** end row *** -->

        <!-- *****  Portfolio Filters ***** -->
        <div class="filters">
            <ul class="wow lightSpeedIn">
                <li><a href="#" data-filter="*" class="active"><i class="icon-grid"></i></a></li>
                <li><a href="#" data-filter=".print">Print Media</a></li>
                <li><a href="#" data-filter=".icon">Icon Design</a></li>
                <li><a href="#" data-filter=".photography">Photography</a></li>
                <li><a href="#" data-filter=".web-design">Web Design</a></li>
                <li><a href="#" data-filter=".ui">UI</a></li>
            </ul>
        </div> <!-- *** end filters *** -->
    </div> <!-- *** end container *** -->

    <!-- *****  Portfolio  wrapper ***** -->
    <div class="portfolio-wrapper margin-bottom-medium">

        <!-- =========================
           Portfolio item
        ============================== -->
        <div class="portfolio-item print">
            <div class="portfolio">
                <a href="img/portfolio/portfolio-1.jpg" data-lightbox-gallery="portfolio">
                    <img src="img/portfolio/portfolio-1.jpg" alt="Portfolio 1"/><!-- END PORTFOLIO IMAGE -->
                    <div class="portfolio-overlay hvr-rectangle-out">
                        <h2 class="margin-bottom-small">
                            Our
                            <strong class="white-color bold-text">Portfolios</strong>
                        </h2>
                        <div class="button">View Project</div>
                    </div><!-- END PORTFOLIO OVERLAY -->
                </a>
           </div>
        </div> <!-- *** end portfolio-item *** -->

        <!-- =========================
           Portfolio item
        ============================== -->
        <div class="portfolio-item photography">
           <div class="portfolio">
              <a href="img/portfolio/portfolio-2.jpg" data-lightbox-gallery="portfolio">
                <img src="img/portfolio/portfolio-2.jpg" alt="Portfolio 2"/><!-- END PORTFOLIO IMAGE -->
                <div class="portfolio-overlay hvr-rectangle-out">
                    <h2 class="margin-bottom-small">
                        Cute
                        <strong class="white-color bold-text">Kittens</strong>
                    </h2>
                    <div class="button">View Project</div>
                </div><!-- END PORTFOLIO OVERLAY -->
              </a>
           </div>
        </div> <!-- *** end  portfolio-item *** -->

        <!-- =========================
           Portfolio item
        ============================== -->
        <div class="portfolio-item photography">
           <div class="portfolio">
              <a href="img/portfolio/portfolio-3.jpg" data-lightbox-gallery="portfolio">
                <img src="img/portfolio/portfolio-3.jpg" alt="Portfolio 1"/><!-- END PORTFOLIO IMAGE -->
                <div class="portfolio-overlay hvr-rectangle-out">
                    <h2 class="margin-bottom-small">
                        Our
                        <strong class="white-color bold-text">Portfolios</strong>
                    </h2>
                    <div class="button">View Project</div>
                </div><!-- END PORTFOLIO OVERLAY -->
              </a>
           </div>
        </div> <!-- *** end  portfolio-item *** -->

        <!-- =========================
           Portfolio item
        ============================== -->
        <div class="portfolio-item print">
           <div class="portfolio">
              <a href="img/portfolio/portfolio-4.jpg" data-lightbox-gallery="portfolio">
                <img src="img/portfolio/portfolio-4.jpg" alt="Portfolio 1"/><!-- END PORTFOLIO IMAGE -->
                <div class="portfolio-overlay hvr-rectangle-out">
                    <h2 class="margin-bottom-small">
                        Our
                        <strong class="white-color bold-text">Portfolios</strong>
                    </h2>
                    <div class="button">View Project</div>
                </div><!-- END PORTFOLIO OVERLAY -->
              </a>
           </div>
        </div> <!-- *** end  portfolio-item *** -->

        <!-- =========================
           Portfolio item
        ============================== -->
        <div class="portfolio-item ui">
           <div class="portfolio">
              <a href="img/portfolio/portfolio-5.jpg" data-lightbox-gallery="portfolio">
                <img src="img/portfolio/portfolio-5.jpg" alt="Portfolio 1"/><!-- END PORTFOLIO IMAGE -->
                <div class="portfolio-overlay hvr-rectangle-out">
                    <h2 class="margin-bottom-small">
                        Our
                        <strong class="white-color bold-text">Portfolios</strong>
                    </h2>
                    <div class="button">View Project</div>
                </div><!-- END PORTFOLIO OVERLAY -->
              </a>
           </div>
        </div> <!-- *** end  portfolio-item *** -->

        <!-- =========================
           Portfolio item
        ============================== -->
        <div class="portfolio-item web-design">
           <div class="portfolio">
              <a href="img/portfolio/portfolio-6.jpg" data-lightbox-gallery="portfolio">
                <img src="img/portfolio/portfolio-6.jpg" alt="Portfolio 1"/><!-- END PORTFOLIO IMAGE -->
                <div class="portfolio-overlay hvr-rectangle-out">
                    <h2 class="margin-bottom-small">
                        Our
                        <strong class="white-color bold-text">Portfolios</strong>
                    </h2>
                    <div class="button">View Project</div>
                </div><!-- END PORTFOLIO OVERLAY -->
              </a>
           </div>
        </div> <!-- *** end  portfolio-item *** -->

        <!-- =========================
           Portfolio item
        ============================== -->
        <div class="portfolio-item web-design">
           <div class="portfolio">
              <a href="img/portfolio/portfolio-7.jpg" data-lightbox-gallery="portfolio">
                <img src="img/portfolio/portfolio-7.jpg" alt="Portfolio 1"/><!-- END PORTFOLIO IMAGE -->
                <div class="portfolio-overlay hvr-rectangle-out">
                    <h2 class="margin-bottom-small">
                        Our
                        <strong class="white-color bold-text">Portfolios</strong>
                    </h2>
                    <div class="button">View Project</div>
                </div><!-- END PORTFOLIO OVERLAY -->
              </a>
           </div>
        </div> <!-- *** end  portfolio-item *** -->

        <!-- =========================
           Portfolio item
        ============================== -->
        <div class="portfolio-item icon">
           <div class="portfolio">
              <a href="img/portfolio/portfolio-8.jpg" data-lightbox-gallery="portfolio">
                <img src="img/portfolio/portfolio-8.jpg" alt="Portfolio 1"/><!-- END PORTFOLIO IMAGE -->
                <div class="portfolio-overlay hvr-rectangle-out">
                    <h2 class="margin-bottom-small">
                        Our
                        <strong class="white-color bold-text">Portfolios</strong>
                    </h2>
                    <div class="button">View Project</div>
                </div><!-- END PORTFOLIO OVERLAY -->
              </a>
           </div>
        </div> <!-- *** end  portfolio-item *** -->
    </div> <!-- *** end  portfolio-wrapper *** -->
    <a href="#" class="button light margin-top-medium margin-bottom-medium hvr-grow"><i class="icon-reload"></i> Load More</a>
</section> <!-- *** end Portfolio *** -->
