<!-- =========================
   Header
============================== -->
<header id="header">
    <div id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
            <li data-target="#myCarousel" data-slide-to="3"></li>
            <!-- <li data-target="#myCarousel" data-slide-to="4"></li> -->
        </ol>

        <!-- Wrapper for Slides -->
        <div class="carousel-inner">

            <!-- *****  Logo ***** -->
            <div class="logo-container">
                <a href="#">
                    <img src="img/logo-header.png" height="139" width="138" alt="">
                </a>
            </div>

            <?php
            echo $this->element(
                'header/slide',
                [
                    'active' => true,
                    'image' => 'slider-1.jpg',
                    'subtitle' => 'Caissons de répétition à Saint-Jean d\'Angely'
                ]
            );
            echo $this->element(
                'header/slide',
                [
                    'active' => false,
                    'image' => 'slider-2.jpg',
                    'subtitle' => 'Association de musiques actuelles à Saint-Jean d\'Angely'
                ]
            );
            echo $this->element(
                'header/slide',
                [
                    'active' => false,
                    'image' => 'slider-3.jpg',
                    'subtitle' => 'Organisateur d\'évènements autour de Saint-Jean d\'Angely'
                ]
            );
            echo $this->element(
                'header/slide',
                [
                    'active' => false,
                    'image' => 'slider-4.jpg',
                    'subtitle' => 'Association musicale et salles de répétition à Saint-Jean d\'Angely'
                ]
            );
            ?>

        </div>

        <!-- Carousel Controls -->
        <a class="left carousel-control hidden-xs" href="#myCarousel" data-slide="prev">
            <span class="icon-prev icon-arrow-left"></span>
        </a>
        <a class="right carousel-control hidden-xs" href="#myCarousel" data-slide="next">
            <span class="icon-next icon-arrow-right"></span>
        </a>
    </div>
</header>
<!-- *** end Header *** -->
