<div class="item <?= isset($active) && $active ? ' active': '' ?>">

    <!-- Set the first background image using inline CSS below. -->
    <div class="fill" style="background-image:url('img/slider/<?= $image ?>');">
    </div>
    <div class="carousel-caption">
        <h1 class="light margin-bottom-medium mab-none"><strong class="bold-text">Blockhouse Musique</strong></h1>
        <p class="light margin-bottom-medium"><?= $subtitle ?></p>
        <div class="call-button">
            <div class="row">
                <div class="col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2 col-xs-12">
                    <a href="#portfolio" class="button pull-right internal-link bold-text light hvr-grow" data-rel="#portfolio">Le Block</a>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <a href="#about-us" class="button pull-left internal-link bold-text main-bg hvr-grow" data-rel="#about-us">Planning</a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="overlay"></div>
</div>
