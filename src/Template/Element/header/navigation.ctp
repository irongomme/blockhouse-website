<!-- =========================
   Fullscreen menu
============================== -->
<div class="mobilenav">
    <ul>
        <li data-rel="#header">
            <span class="nav-label">Accueil</span>
        </li>
        <li data-rel="#block">
            <span class="nav-label">Le Block'</span>
        </li>
        <li data-rel="#bureau">
            <span class="nav-label">Bureau</span>
        </li>
        <li data-rel="#contact">
            <span class="nav-label">Contact</span>
        </li>
        <!-- <li data-rel="#about-us">
            <span class="nav-label">Planning</span>
        </li>

        <li data-rel="#our-team">
            <span class="nav-label">Groupes</span>
        </li>
        <li data-rel="#testimonial">
            <span class="nav-label">Adhésion</span>
        </li>
        <li data-rel="#portfolio">
            <span class="nav-label">Évènements</span>
        </li>
         -->
    </ul>
</div>  <!-- *** end Full Screen Menu *** -->

<!-- *****  hamburger icon ***** -->
<a href="javascript:void(0)" class="menu-trigger">
   <div class="hamburger">
     <div class="menui top-menu"></div>
     <div class="menui mid-menu"></div>
     <div class="menui bottom-menu"></div>
   </div>
</a>
