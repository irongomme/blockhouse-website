<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html class="no-js" >
<head>
    <?= $this->Html->charset() ?>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Association culturelle visant à promouvoir les musiques actuelles à Saint-Jean d'Angely, et organisateur d'évènements musicaux">
    <meta name="keywords" content="association, musiques actuelles, répétition, caisson, concert, festival, jam, saint jean d'angely, fete de la musique, évènement, groupes de musique, orchestre, ecole de musique, formation muticale, locaux, sono, micro, batterie, guitare électrique, chant, basse, clavier, piano, saxophone, trompette, sample, electronique, metal, rock, festif, jazz">
    <meta name="author" content="Blockhouse Musique">

    <title>Blockhouse Musique</title>

    <link rel="apple-touch-icon" sizes="57x57" href="/img/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/img/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/img/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/img/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/img/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="/img/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/img/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="/img/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/img/favicon-16x16.png" sizes="16x16">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Quicksand:400,700' rel='stylesheet' type='text/css'>
    <?= $this->Html->css('website.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->Html->Script('modernizr.min') ?>
    <?= $this->Html->Script('queryloader2.min') ?>

    <?php
        $this->start('script');
        echo $this->Html->Script('jquery.min');
        echo $this->Html->Script('bootstrap.min');
        echo $this->Html->Script('wow.min');
        echo $this->Html->Script('imagesloaded.pkgd.min');
        echo $this->Html->Script('jquery.easing.min');
        echo $this->Html->Script('jquery.appear');
        echo $this->Html->Script('isotope.pkgd.min');
        echo $this->Html->Script('main');
        $this->end();
    ?>
    <!-- =========================
       Preloader
    ============================== -->
    <script>
        window.addEventListener('DOMContentLoaded', function() {
            "use strict";
            new QueryLoader2(document.querySelector("body"), {
                barColor: "#e74c3c",
                backgroundColor: "#111",
                percentage: true,
                barHeight: 1,
                minimumTime: 200,
                fadeOutTime: 1000
            });
        });
        var alert_color_success_background = '#e74c3c';
        var alert_color_error_background = '#CF000F';

    </script>
</head>
<body>

<?php
    echo $this->element('header/navigation');
    echo $this->element('header/header');
    echo $this->Flash->render();
    echo $this->fetch('content') ;
?>

    <!-- =========================
       Footer
    ============================== -->
    <footer id="footer" class="footer">
        <div class="container padding-large text-center">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <figure class="margin-bottom-medium">
                        <img src="img/logo.png" class="footer-logo" alt="Krefolio">
                    </figure>
                    <p class="margin-bottom-medium">Le site blockhousemusique.org est édité par
                    l’association BLOCKHOUSE MUSIQUE, association loi 1901 déclarée en préfecture de
                    Charente-Maritime, inscrite au Répertoire National des Associations sous le
                    n° WXXXXXXXXX et publiée au J.O du JJ MMMM YYYY</p>

                    <!-- =========================
                       Social icons
                    ============================== -->
                    <ul class="social margin-bottom-medium">
                        <li class="facebook hvr-pulse"><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li class="g-plus hvr-pulse"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    </ul>
                    <p class="copyright">
                        &copy; Copyright <?= date('Y') ?> BLOCKHOUSE MUSIQUE - Droits réservés
                    </p>
                </div>
            </div>
        </div>
    </footer>
    <!-- *** end Footer *** -->

    <!-- =========================
       Back to top button
    ============================== -->
    <div class="back-to-top" data-rel="header">
        <i class="icon-up"></i>
    </div>

    <?= $this->fetch('script') ?>
</body>
</html>
