<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Member Entity.
 *
 * @property int $id
 * @property string $name
 * @property string $alias
 * @property bool $is_board_member
 * @property string $board_role
 * @property bool $is_musician
 * @property bool $is_volunteer
 * @property string $mobile
 * @property string $phone
 * @property string $email
 * @property \Cake\I18n\Time $birthday
 * @property bool $has_room_keys
 * @property bool $has_board_keys
 * @property string $comment
 * @property bool $is_active
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \Cake\I18n\Time $disabled
 * @property \App\Model\Entity\Message[] $messages
 * @property \App\Model\Entity\Subscription[] $subscriptions
 * @property \App\Model\Entity\TlMemberToGroup[] $tl_member_to_group
 * @property \App\Model\Entity\User[] $users
 * @property \App\Model\Entity\ArchiveBand[] $archive_bands
 * @property \App\Model\Entity\BandsMembersInstrument[] $bands_members_instruments
 * @property \App\Model\Entity\Band[] $bands
 * @property \App\Model\Entity\Supply[] $supplies
 */
class Member extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
