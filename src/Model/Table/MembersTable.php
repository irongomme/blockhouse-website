<?php
namespace App\Model\Table;

use App\Model\Entity\Member;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Members Model
 *
 * @property \Cake\ORM\Association\HasMany $Messages
 * @property \Cake\ORM\Association\HasMany $Subscriptions
 * @property \Cake\ORM\Association\HasMany $TlMemberToGroup
 * @property \Cake\ORM\Association\HasMany $Users
 * @property \Cake\ORM\Association\BelongsToMany $ArchiveBands
 * @property \Cake\ORM\Association\BelongsToMany $BandsMembersInstruments
 * @property \Cake\ORM\Association\BelongsToMany $Bands
 * @property \Cake\ORM\Association\BelongsToMany $Supplies
 */
class MembersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('members');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Messages', [
            'foreignKey' => 'member_id'
        ]);
        $this->hasMany('Subscriptions', [
            'foreignKey' => 'member_id'
        ]);
        $this->hasMany('TlMemberToGroup', [
            'foreignKey' => 'member_id'
        ]);
        $this->hasMany('Users', [
            'foreignKey' => 'member_id'
        ]);
        $this->belongsToMany('ArchiveBands', [
            'foreignKey' => 'member_id',
            'targetForeignKey' => 'archive_band_id',
            'joinTable' => 'archive_bands_members'
        ]);
        $this->belongsToMany('BandsMembersInstruments', [
            'foreignKey' => 'member_id',
            'targetForeignKey' => 'bands_members_instrument_id',
            'joinTable' => 'archive_bands_members_instruments'
        ]);
        $this->belongsToMany('Bands', [
            'foreignKey' => 'member_id',
            'targetForeignKey' => 'band_id',
            'joinTable' => 'bands_members'
        ]);
        $this->belongsToMany('Supplies', [
            'foreignKey' => 'member_id',
            'targetForeignKey' => 'supply_id',
            'joinTable' => 'members_supplies'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->allowEmpty('alias');

        $validator
            ->boolean('is_board_member')
            ->requirePresence('is_board_member', 'create')
            ->notEmpty('is_board_member');

        $validator
            ->allowEmpty('board_role');

        $validator
            ->boolean('is_musician')
            ->requirePresence('is_musician', 'create')
            ->notEmpty('is_musician');

        $validator
            ->boolean('is_volunteer')
            ->requirePresence('is_volunteer', 'create')
            ->notEmpty('is_volunteer');

        $validator
            ->allowEmpty('mobile');

        $validator
            ->allowEmpty('phone');

        $validator
            ->email('email')
            ->allowEmpty('email');

        $validator
            ->date('birthday')
            ->allowEmpty('birthday');

        $validator
            ->boolean('has_room_keys')
            ->requirePresence('has_room_keys', 'create')
            ->notEmpty('has_room_keys');

        $validator
            ->boolean('has_board_keys')
            ->requirePresence('has_board_keys', 'create')
            ->notEmpty('has_board_keys');

        $validator
            ->allowEmpty('comment');

        $validator
            ->boolean('is_active')
            ->requirePresence('is_active', 'create')
            ->notEmpty('is_active');

        $validator
            ->dateTime('disabled')
            ->allowEmpty('disabled');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        return $rules;
    }
}
